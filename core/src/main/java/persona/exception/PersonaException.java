package persona.exception;

import persona.model.Persona;

public class PersonaException extends RuntimeException{
    public PersonaException(String msg){
        super(msg);
    }
}
