package persona.output;

import persona.model.Persona;

import java.util.Collection;

public interface BuscarPersonasRepository{
    Collection<Persona> buscarPersonas();
}
