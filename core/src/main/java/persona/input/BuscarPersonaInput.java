package persona.input;

import persona.model.Persona;

public interface BuscarPersonaInput {
    Persona buscarPersona(Integer id);
}
