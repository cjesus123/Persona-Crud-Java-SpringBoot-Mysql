package persona.input;

import persona.usecase.actualizarPersonaUseCase.ActualizarPersonaRequestModel;

public interface ActualizarPersonaInput {
    boolean actualizarPersona(ActualizarPersonaRequestModel actualizarPersonaRequestModel);
}
