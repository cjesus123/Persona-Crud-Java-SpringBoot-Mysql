package persona.usecase.actualizarPersonaUseCase;

import persona.exception.PersonaException;
import persona.input.ActualizarPersonaInput;
import persona.model.Persona;
import persona.output.ActualizarPersonaRepository;
import persona.output.BuscarPersonaRepository;

public class ActualizarPersonaUseCase implements ActualizarPersonaInput{

    ActualizarPersonaRepository actualizarPersonaRepository;
    BuscarPersonaRepository buscarPersonaRepository;

    public ActualizarPersonaUseCase(ActualizarPersonaRepository actualizarPersonaRepository,BuscarPersonaRepository buscarPersonaRepository) {
        this.actualizarPersonaRepository = actualizarPersonaRepository;
        this.buscarPersonaRepository = buscarPersonaRepository;
    }

    @Override
    public boolean actualizarPersona(ActualizarPersonaRequestModel actualizarPersonaRequestModel) {
        if(buscarPersonaRepository.buscarPersona(actualizarPersonaRequestModel.getId()).isEmpty()){
            throw new PersonaException("La persona no existe");
        }
        return actualizarPersonaRepository.actualizarPersona(Persona.getInstance(actualizarPersonaRequestModel.getId(),
                actualizarPersonaRequestModel.getNombre(),
                actualizarPersonaRequestModel.getApellido(),
                actualizarPersonaRequestModel.getDni(),
                actualizarPersonaRequestModel.getFechaNacimiento()));
    }
}
