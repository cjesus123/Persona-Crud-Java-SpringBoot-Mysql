package persona.usecase;

import persona.exception.PersonaException;
import persona.input.BuscarPersonaInput;
import persona.model.Persona;
import persona.output.BuscarPersonaRepository;

import java.util.Collection;

public class BuscarPersonaUseCase implements BuscarPersonaInput {

    BuscarPersonaRepository buscarPersonaRepository;

    public BuscarPersonaUseCase(BuscarPersonaRepository buscarPersonasRepository) {
        this.buscarPersonaRepository = buscarPersonasRepository;
    }

    @Override
    public Persona buscarPersona(Integer id){
        if(buscarPersonaRepository.buscarPersona(id).isEmpty())
            throw new PersonaException("La persona no existe");
        return buscarPersonaRepository.buscarPersona(id).get();
    }
}
