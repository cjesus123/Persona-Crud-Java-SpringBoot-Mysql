package persona.usecase.registrarPersonaUseCase;

import persona.exception.PersonaException;
import persona.input.RegistrarPersonaInput;
import persona.model.Persona;
import persona.output.RegistrarPersonaRepository;

public class RegistrarPersonaUseCase implements RegistrarPersonaInput {
    RegistrarPersonaRepository registrarPersonaRepository;

    public RegistrarPersonaUseCase(RegistrarPersonaRepository registrarPersonaRepository) {
        this.registrarPersonaRepository = registrarPersonaRepository;
    }
    @Override
    public Integer registrarPersona(RegistrarPersonaRequestModel registrarPersonaRequestModel){
        if(registrarPersonaRepository.existePersona(registrarPersonaRequestModel.getNombre(),registrarPersonaRequestModel.getApellido()))
            throw new PersonaException("La persona ya existe");
        Persona persona = Persona.getInstance(null,
                registrarPersonaRequestModel.getNombre(),
                registrarPersonaRequestModel.getApellido(),
                registrarPersonaRequestModel.getDni(),
                registrarPersonaRequestModel.getFechaNacimiento()
                );
        return registrarPersonaRepository.registrarPersona(persona);
    }
}
