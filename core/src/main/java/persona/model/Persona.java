package persona.model;

import persona.exception.PersonaException;

import java.time.LocalDate;

public class Persona{

    private Integer id;
    private String nombre;
    private String apellido;
    private String dni;
    private LocalDate fechaNacimiento;

    private Persona(Integer id,String nombre, String apellido, String dni, LocalDate fechaNacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.fechaNacimiento = fechaNacimiento;
    }

    public static Persona getInstance(Integer id,String nombre, String apellido, String dni, LocalDate fechaNacimiento){
        if(nombre == null || nombre.isBlank())
            throw new PersonaException("El nombre no es valido");
        if(apellido == null || apellido.isBlank())
            throw new PersonaException("El apellido no es valido");
        if(dni == null || dni.isBlank() || dni.length()<8)
            throw new PersonaException("El dni no es valido");
        if(fechaNacimiento.isAfter(LocalDate.now()))
            throw new PersonaException("La fecha de nacimiento no es valida");
        return new Persona(id,nombre,apellido,dni,fechaNacimiento);
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
}
