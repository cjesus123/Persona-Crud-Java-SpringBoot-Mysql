package ar.edu.undec.adapter.service.persona.controllers;

import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import persona.input.BuscarPersonaInput;

@RestController
@RequestMapping("personas")
@CrossOrigin("*")
public class BuscarPersonaController{

    BuscarPersonaInput buscarPersonaInput;

    public BuscarPersonaController(BuscarPersonaInput buscarPersonaInput) {
        this.buscarPersonaInput = buscarPersonaInput;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> buscarPersona(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(buscarPersonaInput.buscarPersona(id));
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
