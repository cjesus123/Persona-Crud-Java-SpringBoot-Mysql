package ar.edu.undec.adapter.data.persona.repoImplementation;

import ar.edu.undec.adapter.data.persona.crud.BuscarPersonasCRUD;
import ar.edu.undec.adapter.data.persona.mapper.PersonaDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.model.Persona;
import persona.output.BuscarPersonasRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class BuscarPersonasRepoImplementation implements BuscarPersonasRepository {

    BuscarPersonasCRUD buscarPersonasCrud;

    @Autowired
    public BuscarPersonasRepoImplementation(BuscarPersonasCRUD buscarPersonasCrud) {
        this.buscarPersonasCrud = buscarPersonasCrud;
    }
    @Override
    @Transactional
    public Collection<Persona> buscarPersonas() {
        return buscarPersonasCrud.findAll().stream().map(PersonaDataMapper::dataCoreMapper).collect(Collectors.toList());
    }
}
