package ar.edu.undec.adapter.data.persona.repoImplementation;

import ar.edu.undec.adapter.data.persona.crud.BuscarPersonaCRUD;
import ar.edu.undec.adapter.data.persona.mapper.PersonaDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.model.Persona;
import persona.output.BuscarPersonaRepository;
import persona.output.BuscarPersonasRepository;

import java.util.Collection;
import java.util.Optional;

@Service
public class BuscarPersonaRepoImplementation implements BuscarPersonaRepository {

    BuscarPersonaCRUD buscarPersonaCRUD;

    @Autowired
    public BuscarPersonaRepoImplementation(BuscarPersonaCRUD buscarPersonaCRUD) {
        this.buscarPersonaCRUD = buscarPersonaCRUD;
    }

    @Override
    public Optional<Persona> buscarPersona(Integer id) {
        return buscarPersonaCRUD.findById(id).map(PersonaDataMapper::dataCoreMapper);
    }
}
