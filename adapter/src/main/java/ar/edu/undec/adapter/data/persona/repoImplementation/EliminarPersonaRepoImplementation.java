package ar.edu.undec.adapter.data.persona.repoImplementation;

import ar.edu.undec.adapter.data.persona.crud.EliminarPersonaCRUD;
import ar.edu.undec.adapter.data.persona.exception.DataBaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.output.EliminarPersonaRepository;

import javax.transaction.Transactional;

@Service
public class EliminarPersonaRepoImplementation implements EliminarPersonaRepository {

    EliminarPersonaCRUD eliminarPersonaCRUD;

    @Autowired
    public EliminarPersonaRepoImplementation(EliminarPersonaCRUD eliminarPersonaCRUD) {
        this.eliminarPersonaCRUD = eliminarPersonaCRUD;
    }
    @Override
    @Transactional
    public boolean eliminarPersona(Integer id){
        try {
            eliminarPersonaCRUD.deleteById(id);
            return true;
        }catch (RuntimeException e){
            throw new DataBaseException("Error al eliminar la persona de la base de datos");
        }
    }
}
