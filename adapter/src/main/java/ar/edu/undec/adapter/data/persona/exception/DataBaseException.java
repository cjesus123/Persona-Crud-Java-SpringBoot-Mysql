package ar.edu.undec.adapter.data.persona.exception;

public class DataBaseException extends RuntimeException{
    public DataBaseException(String msg){
        super(msg);
    }
}
