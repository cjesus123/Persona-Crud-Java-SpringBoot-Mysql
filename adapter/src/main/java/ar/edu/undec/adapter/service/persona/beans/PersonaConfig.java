package ar.edu.undec.adapter.service.persona.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import persona.input.*;
import persona.output.*;
import persona.usecase.BuscarPersonaUseCase;
import persona.usecase.BuscarPersonasUseCase;
import persona.usecase.EliminarPersonaUseCase;
import persona.usecase.actualizarPersonaUseCase.ActualizarPersonaUseCase;
import persona.usecase.registrarPersonaUseCase.RegistrarPersonaUseCase;

@Configuration
public class PersonaConfig{

    @Bean
    RegistrarPersonaInput registrarPersonaInput(RegistrarPersonaRepository registrarPersonaRepository){
        return new RegistrarPersonaUseCase(registrarPersonaRepository);
    }

    @Bean
    BuscarPersonasInput buscarPersonasInput(BuscarPersonasRepository buscarPersonasRepository) {
        return new BuscarPersonasUseCase(buscarPersonasRepository);

    }
    @Bean
    BuscarPersonaInput buscarPersonaInput(BuscarPersonaRepository buscarPersonaRepository){
        return new BuscarPersonaUseCase(buscarPersonaRepository);
    }

    @Bean
    EliminarPersonaInput eliminarPersonaInput(EliminarPersonaRepository eliminarPersonaRepository,BuscarPersonaRepository buscarPersonaRepository){
        return new EliminarPersonaUseCase(eliminarPersonaRepository,buscarPersonaRepository);
    }

    @Bean
    ActualizarPersonaInput actualizarPersonaInput(ActualizarPersonaRepository actualizarPersonaRepository,BuscarPersonaRepository buscarPersonaRepository){
        return new ActualizarPersonaUseCase(actualizarPersonaRepository,buscarPersonaRepository);
    }
}
