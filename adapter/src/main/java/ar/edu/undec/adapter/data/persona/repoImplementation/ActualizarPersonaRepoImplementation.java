package ar.edu.undec.adapter.data.persona.repoImplementation;

import ar.edu.undec.adapter.data.persona.crud.ActualizarPersonaCRUD;
import ar.edu.undec.adapter.data.persona.exception.DataBaseException;
import ar.edu.undec.adapter.data.persona.mapper.PersonaDataMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import persona.model.Persona;
import persona.output.ActualizarPersonaRepository;

@Service
public class ActualizarPersonaRepoImplementation implements ActualizarPersonaRepository {

    ActualizarPersonaCRUD actualizarPersonaCRUD;

    @Autowired
    public ActualizarPersonaRepoImplementation(ActualizarPersonaCRUD actualizarPersonaCRUD) {
        this.actualizarPersonaCRUD = actualizarPersonaCRUD;
    }
    @Override
    public boolean actualizarPersona(Persona persona) {
        try{
            actualizarPersonaCRUD.save(PersonaDataMapper.coreDataMapper(persona));
            return true;
        }catch (RuntimeException e){
            throw new DataBaseException("Error al actualizar la persona");
        }
    }
}
