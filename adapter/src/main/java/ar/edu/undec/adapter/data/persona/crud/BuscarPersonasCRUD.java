package ar.edu.undec.adapter.data.persona.crud;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface BuscarPersonasCRUD extends CrudRepository<PersonaEntity,Integer>{
    Collection<PersonaEntity> findAll();
}
