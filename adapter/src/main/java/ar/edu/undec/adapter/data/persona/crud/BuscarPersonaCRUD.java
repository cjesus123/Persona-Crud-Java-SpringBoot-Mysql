package ar.edu.undec.adapter.data.persona.crud;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BuscarPersonaCRUD extends CrudRepository<PersonaEntity,Integer>{
    Optional<PersonaEntity> findById(Integer id);
}
